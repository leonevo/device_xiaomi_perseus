Copyright (C) 2018 The LineageOS Project

Device configuration for Xiaomi Mi Mix3 [WIP]
=========================================

The Xiaomi Mi Mix 3 (codenamed _"perseus"_) is a high-end smartphone from Xiaomi.

It was announced in October 2018. Release date was November 2018.

## Device specifications

Basic   | Spec Sheet
-------:|:-------------------------
SoC     | Qualcomm SDM845 Snapdragon 845
CPU     | Octa-core (4x2.8 GHz Kryo 385 Gold & 4x1.8 GHz Kryo 385 Silver)
GPU     | Adreno 630
Memory  | 6 GB RAM
Shipped Android Version | 9 with MIUI 10
Storage | 64/128/256 GB
Battery | Non-removable Li-Ion 320 mAh battery
Display | 1080 x 2340 pixels, 18:9 ratio, 6.21 inches, Super AMOLED (~403 ppi density)
Camera  | Dual 12 MP, 4-axis OIS, 2x optical zoom, dual PDAF, dual-LED (dual tone) flash

## Device picture

![Xiaomi Mi Mix 3](https://xiaomi-mi.com/uploads/CatalogueImage/01b_17242_1540477917.jpg "Xiaomi Mi Mix 3 in black")
